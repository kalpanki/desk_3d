clc; clear all;

me = mfilename;

mydir = which(me); 
mydir = mydir(1:end-2-numel(me));

addpath(mydir(1:end-1));
addpath([mydir,'calib_light']);
addpath([mydir,'images']);
addpath([mydir,'my_data']);
addpath([mydir,'my_data/calib_images']);
addpath([mydir,'my_data/images']);

disp('Project setup done. Run main.m to see some results !!!');