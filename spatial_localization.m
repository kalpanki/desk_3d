%
% for each image, find the shadow edge or boundary pixel
% Spatio-temporal Thresholding
%

clc; clear all; close all;

%% if we haven't calibrated yet, we need to

if (~exist('calib_params','var'))
    calibrate_1;
end
   

%% some constants we will use
contThreshold = 40;
% y_top = 10;
% y_bot = 250; 
% without resizing - for our images
y_top = 550; 
y_bot = 1400;
% with resizing - for our images
%y_top = 185;
%y_bot = 340;

tMin = 1;
%tMax = 39; % no of images
%tMax = 26;
tMax = 25;


%% Pre processing

% read all images, and crop them and
% store in variable I
I = {};
for j = 1:tMax
    if (j < 10)
        %name = strcat('images/bottle00',num2str(j),'.pgm'); 
    else
        % for given dat
        %name = strcat('images/bottle0',num2str(j),'.pgm'); 
        % for our data
        %name = strcat('my_data/images/image',num2str(j),num2str(0),'.jpg');
    end
    % for our data
    name = strcat('my_data/images/image',num2str(j),num2str(0),'.jpg');
    % smoothing to get rid of some noise
    hdl = fspecial('gaussian',[10 10]);
    img = imread(name);
    %img_1 = imcrop(img,[100 150 350 350]);
    res = imfilter(img,hdl);
    % not suggested in the paper. but actually helps 
    I{j} = histeq( res );
end

%% compute all needed intensity images

% size of images - all assumed to be of same size
[y x] = size(I{1});
IThresh = contThreshold*ones(y,x);

IMin = I{1};
IMax = I{1};
for i = 2:tMax
    IMin = min(I{i},IMin);
    IMax = max(I{i},IMax);
end

IShadow = (IMax + IMin)/2;
IContrast = IMax - IMin;

for i=1:tMax
    del_I{i} = double(I{i}) - double(IShadow);
end

%% spatial localization - top row

% careful setting to 1 - matlab blows out of memory 
% if plotting for all frames!
do_plot = 0;
% if unhappy with results, set to 1 
% and click on shadow crossing points interactively
do_manual = 0;

for i = 1:tMax-1
    if (do_manual == 1)
        figure; imshow(del_I{i},[]); hold on;
        line([1 1300],[y_top y_top]);
        drawnow; 
        [x,y] = ginput(1);
        plot(x,y,'r*');
        % pixel coordinates
        x_top(i) = x;
        hold off; close;
    else
        x_top(i) = getZeroCross(del_I{i},y_top,do_plot);
    end
end


%% spatial localization - bottom row

% careful setting to 1 - matlab blows out of memory 
% if plotting for all frames!
do_plot = 0;
% if unhappy with results, set to 1 
% and click on shadow crossing points interactively
do_manual = 0;
for i = 1:tMax-1
    if (do_manual == 1)
        figure; imshow(del_I{i},[]); hold on;
        line([1 1300],[y_bot y_bot]);
        drawnow; 
        [x,y] = ginput(1);
        plot(x,y,'r*');
        % pixel coordinates
        x_bot(i) = x;
        hold off; close;
    else
        x_bot(i) = getZeroCross(double(del_I{i}),y_bot, do_plot);
    end
    
end


%% Temporal Localization - calculating shadow time, ts(xc_bar) 
% finding the zero crossing time for all pixels

[m n] = size(I{1});
ts = zeros(m,n);

for row = 1:m
    for col = 1:n
        if (IContrast(row,col) < 70) 
            % do nothing
            continue;
        end

        for i=1:tMax
            % consider a single pixel, say (100,300)
            vals(i) = double(del_I{i}(row,col));
        end
        
        
        % calculate del_t
        j = 1; del_t = []; t0 = [];
        for i=2:length(vals)
            % zero crossing - going from +ve to -ve
            if (vals(i) > 0 && vals(i-1) <= 0)
                del_t(j) = double(vals(i) / double(vals(i) - vals(i-1)));
                t0(j) = i;
                j = j+1;
            end
        end
        
        if (isempty(del_t))
            continue;
        end
        
        if (length(del_t) > 1)
            %disp('too many del_t values');
            continue;
        else 
            temp = double( t0(1) - del_t(1));
            %fprintf('updating row %i col %i with val: %f \n',row,col,temp);
            ts(row,col) = temp;
        end
        
    end
end


%% Validation - zero crossing of temporal values obtained
% Just to see if we are getting the right profile - Can be skipped
% choose some random row and column for which we have 
% calculated ts values

[m n] = size(I{1});

row = 1920; col = 800;
for i=1:tMax
    % consider a single pixel, say (100,300)
    vals(i) = double(del_I{i}(row,col));
end
plot(vals); hold on;
plot(ts(row,col),0,'r*')



%% Triangulation using shadow crossing time ts and 
% reference points x_top, x_bot, y_top, y_bot.
% !!! Takes quite a while - 3-4 Mins !!!
%
% From calibration we have:
% desk     : desk plane parameters
% desk_dist: distance between the desk and the camera - C

ts_uniq = unique(ts);
% interpolate reference points to match ts
x_top_ipl = interp1(1:tMax-1,x_top(1:tMax-1),ts_uniq);
x_bot_ipl = interp1(1:tMax-1,x_bot(1:tMax-1),ts_uniq);
% create a mapping now
% key =  ts_uniq, value = x_top_ipl,
x_top_map = containers.Map(ts_uniq,x_top_ipl);
x_bot_map = containers.Map(ts_uniq,x_bot_ipl);


% we bother about pixels only within the 
% reference rows
k = 1;
fileID = fopen('result.txt','w');
fprintf(fileID,'%d \n',m-y_top);
fprintf(fileID,'%d \n',n);

% from top ref to bottom ref row is enough
for row = y_top:y_bot 
    for col = 1:n
        % get the shadow plane using the ref points & calib params
        shadow_plane = getShadowPlane(row,col,ts,x_top_map, x_bot_map, ...
                                    y_top, y_bot, calib_params);

        if (isnan(shadow_plane(1)))
            point_3d(k,:) = [0.0 0.0 0.0];
        else
            % Now that we have the shadow plane for the pixel, use it. 
            % 3D point is the intersection of shadow plane and 
            % ray through the pixel
            pix_pt = toCamCoord([row col 1], calib_params);
            point_3d(k,:) = vector_plane_intersect(shadow_plane, pix_pt);
        end
        % save the points to result.txt file
        fprintf(fileID,'%f %f %f 0\n',point_3d(k,1),point_3d(k,2),point_3d(k,3));
        k = k+1;
    end
end

fclose(fileID);

disp('DONE with triangulation :-)');
disp('3D points saved in result.txt');


