
videoFReader = ...
    vision.VideoFileReader('my_data/video_4.mp4');

videoPlayer = vision.VideoPlayer;

i = 0;
while ~isDone(videoFReader)
    videoFrame = step(videoFReader);
    % a sample to see we are doing ok
    if (i == 10)
        imshow(videoFrame);
        title('Sample image from frame');
    end
    % for every 10th frame, 
    if (rem(i,10) == 0)
        % convert and save it
        name = strcat('my_data/images/image',num2str(i),'.jpg');
        %imwrite(imresize(rgb2gray(videoFrame), 0.25), name, 'JPEG');
        imwrite(rgb2gray(videoFrame), name, 'JPEG');
        fprintf('.');
    end
    % move ahead
    step(videoPlayer, videoFrame);
    i = i+1;
end
fprintf('\n Done converting video to images !!! \n');

release(videoPlayer);
release(videoFReader);