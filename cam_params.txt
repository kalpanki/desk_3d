
Extrinsic parameters for image1.jpg:

Translation vector: Tc_ext = [ -46.488307 	 -38.161080 	 289.310214 ]
Rotation vector:   omc_ext = [ 1.632280 	 1.492645 	 -0.933494 ]
Rotation matrix:    Rc_ext = [ 0.065449 	 0.997060 	 -0.039842
                               0.472227 	 -0.066122 	 -0.878994
                               -0.879044 	 0.038715 	 -0.475166 ]
Pixel error:           err = [ 10.16473 	 10.35841 ]


Extrinsic parameters for image2.jpg:

Extrinsic parameters:

Translation vector: Tc_ext = [ -37.741962 	 -36.065210 	 255.841511 ]
Rotation vector:   omc_ext = [ 1.552077 	 1.632303 	 -0.950519 ]
Rotation matrix:    Rc_ext = [ -0.054744 	 0.998471 	 -0.007614
                               0.499415 	 0.020778 	 -0.866114
                               -0.864632 	 -0.051217 	 -0.499789 ]
Pixel error:           err = [ 1.40944 	 2.49499 ]


Intrinsics:


Focal Length:          fc = [ 2632.73127   2545.30342 ] ± [ 598.12644   796.87694 ]
Principal point:       cc = [ 1183.50000   1599.50000 ] ± [ 0.00000   0.00000 ]
Skew:             alpha_c = [ 0.00000 ] ± [ 0.00000  ]   => angle of pixel axes = 90.00000 ± 0.00000 degrees
Distortion:            kc = [ 0.00778   0.37237   0.00045   0.00123  0.00000 ] ± [ 0.09115   0.62747   0.00627   0.00578  0.00000 ]
Pixel error:          err = [ 0.87674   1.48576 ]

fc - pixels



Xd =

    2.7905    0.2852    1.0000
    2.7516    0.2927    1.0000


    Xd =

    2.7774    0.2886    1.0000
    2.7774    0.2886    1.0000