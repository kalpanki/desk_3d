function x_location = getZeroCross(I, row, do_plot)
% I - on which image
% row - row in the image to consider. 
%       In this case it will be rows 20 and 230.


    % get the x_top and x_bot reference points
    % top profile
    top_row = I(row,:);
    [m n] = size(I);
    
    
    if (do_plot) figure; imshow(I,[]); hold on; end
    
    
    % interpoloate the intensity values in the row
    %x_vals = 50:0.01:600;
    x_vals = 1:0.01:n;
    y_vals = interp1(1:n,top_row(1:n),x_vals);

    % we want the last index where we cross from
    % -ve to +ve
    j=1; x_idx = [];
    for i=2:length(y_vals)
        if ((y_vals(i-1) < 0 && y_vals(i) >= 0))
            x_idx(j) = i;
            j = j+1;
        end
    end

    % find the actual zero crossing
    if (isempty(x_idx))
        warning('There is no zero crossing found !!');
        x_location = 0;
    else
        x_location = x_vals(max(x_idx));
        if (do_plot)
            for i = 1:length(x_idx)
                %line([1 3000],[550 550]);
                line([1 3000],[1400 1400]);
                plot(x_location,row,'r*');
            end
            hold off;
        end
    end
    
    % for validation purpose, plot the profile
%     figure; plot(top_row); hold on;
%     plot(x_location,I(int8(x_location),int8(row)),'r*');
%     title('Intensity profile');
%     xlabel('column pixels - x');
%     ylabel('Normalized intensity');
%     hold off;

end