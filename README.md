3D photography on desk
======================
This is an implementation of the [paper](http://www.cs.cmu.edu/~seitz/course/SIGG99/papers/boug98.pdf) to generate 3D point clouds from images of structured light (shadow) scanning a given scene.

The original report of this work can be found on my website [here](http://www.ssankar.org/uploads/4/7/8/0/4780638/_sankar_li_3d_report.pdf).

Requirements
------------
- Matlab software with Computer Vision and Image processing toolbox.
- Bouguet's Camera Calibration [toolbox](http://www.vision.caltech.edu/bouguetj/calib_doc/)
- A machine with at least 4GB of RAM (it takes about 4-5 mins on a machine with 8GB RAM, Intel Core i7 processor). Due to the difficulty with extracting frames from videos in Linux, a Mac machine is prefereable.
- A video of the scanning sequence.

Provided data
-------------
- All data of our work is present in `my_data` folder. 
- Our calibration images are provided in `my_data/calib_data` folder.
- Sample videos provided are: 
* video_3.mp4: scan of a lock
* video_4.mp4: scan of a stapeller
* video.mp4: scan of a plug
- Frames/images already extracted from the stapeller video are provided in `my_data/images`.

Steps to follow
----------------
- Run the `<startup.m>` to add folders to the Matlab path.
- Calibrate the camera used to take the video sequence using the Bouguet's toolbox or similar.
- Export the result as `<Calib_Results.mat>`.
- Convert the video sequence into image frames using the `<convert.m>` file. The extracted frames get stored in `my_data/images` folder.
- Run `<main.m>` file. This will in turn trigger light source calibration step.
- Choose the points on the source calibration images interactively
- Wait for the results to be stored in `<result.txt>` file.
- Visualize the points generated using the java tool that comes with Paul Blaer's implementation [code](http://www.cs.columbia.edu/~pblaer/projects/photo/shadow_scanning.tgz).
- Voila! :smiley: