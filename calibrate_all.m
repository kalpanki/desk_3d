% calibrate_1 : This is the main calibration script for our method.
%
% This script gets called as part of the main program, as the first step.
% However, this can also be called separately.
% The results in K matrix are in pixels and not in metric.


clc; clear all; close all;

%% Camera calibration

if (exist('Calib_Results.mat','file') == 2)
    temp = load('Calib_Results.mat','KK');
    calib_params.int = temp.KK;
else
    disp('Cannot calibrate. There is no calibration file existing.');
    disp('Please calibrate and store it in Calib_Results.mat');
    return;
end

% lets get the needed intrinsics
K = calib_params.int;
Cx = K(1,3); Cy = K(2,3);
fx = K(1,1); fy = K(2,2);

Rc_ext = load('Calib_Results.mat','Rc_1');
Rc_ext = Rc_ext.Rc_1;
Tc_ext = load('Calib_Results.mat','Tc_1');
Tc_ext = Tc_ext.Tc_1;
calib_params.ext = [Rc_ext Tc_ext];
calib_params.ext(4,:) = [0 0 0 1];
M = calib_params.ext;


%% Source calibration - to find the location of the light source
% It is assumed that the light calibration images are found in the folder: 
% my_data/calib_images

% known fact
% height of the pen cap in mm.
pencilHeight = 17;
% number of images to use for source calibration
num_images = 4;
num_pts = 2;

for j = 1:num_images
    temp = strcat('my_data/calib_images/light-calibration',num2str(j),'.jpg');
    I = imread(temp);
    figure; imshow(I,[]);
    % 1 - top tip of shadow
    % 2 - bottom tip of shadow
    for i=1:num_pts
       drawnow; hold on;
       [x,y] = ginput(1);
       plot(x,y,'r*');
       text_handle = text( x+5, y, num2str(i) );
       set(text_handle,'Color',[1 1 1])
       % pixel coordinates
       image(j).pts(i,1:2) = [x,y];
    end
    %waitforbuttonpress;
    close;
end
h = gcf;
close(h);


% -------------------------------------------------------------------------
% We now have pencil points in image coordinates

% The normal to the desk plane is the last column
desk_norm = [calib_params.ext(1,3) calib_params.ext(2,3) calib_params.ext(3,3)];

% perpendicular distance between camera and desk plane
desk_dist = dot(Tc_ext, desk_norm);
fprintf('desk dist: %f \n',desk_dist);

% we need it for triangulation. so add to calib params
calib_params.desk_dist = desk_dist;
calib_params.desk_norm = desk_norm;

% define the desk plane using the norm and distance
desk(1:3) = desk_norm(1:3);
desk(4) = desk_dist / sqrt(desk_norm(1)^2 + desk_norm(2)^2 + desk_norm(2)^2);
fprintf('desk plane defined: %f \n',desk);

% add to calibration params as we need it for triangulation
calib_params.desk = desk;
calib_params.f = fx;
calib_params.Cx = Cx;
calib_params.Cy = Cy;


for j = 1:num_images
    % 3D point corresponding to pencil shadow (bottom)
    % in image reference frame
    pt = [image(j).pts(2,:) 1]';
    
    % now, x = K[I|0]X_cam
    % by inverting, we can go from image to camera reference
    temp = [K zeros(3,1)];
    pt1_cam = pinv(temp)*pt;
    
    shadowBottom3d(:,j) = vectorPlaneIntersect(desk, pt1_cam);

    % 3D point corresponding to pencil shadow (top)
    % in image reference frame
    pt = [image(j).pts(1,:) 1]';
    pt2_cam = pinv(temp)*pt;
    
    shadowTop3d(:,j) = vectorPlaneIntersect(desk, pt2_cam);

    % 3D point corresponding to actual pencil top tip
    % in our case, the top tip of the cap
    pencilTop3d(:,j) = shadowBottom3d(:,j) + pencilHeight * desk_norm';
end

fprintf('shadow top are: %f, %f, %f, \n', shadowTop3d);
fprintf('pencil top are: %f, %f, %f, \n', pencilTop3d);

% intersection point of lines formed by 2 pair of points
[pa pb] = lineLineIntersect(shadowTop3d, pencilTop3d);

calib_params.source = (pa+pb) / 2;
fprintf('source location is: %f, %f, %f, \n', calib_params.source);
disp('Done with Calibration!');
