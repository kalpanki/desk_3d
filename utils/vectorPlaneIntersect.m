function point_3d = vectorPlaneIntersect(plane, vector)
% finds the point of intersection of a
% vector and a plane

    if (size(vector,1) == 1)
        denom = plane(1:3)*vector(1:3)';
    else
        denom = plane(1:3)*vector(1:3);
    end
    k = plane(4) / denom;
    point_3d = k*vector(1:3);

end