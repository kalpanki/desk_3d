function [ plane ] = getPlaneFromPoints( p1, p2, p3 )
%GETPLANEFROMPOINTS: Given 3 points, returns back the plane that is formed
% p1, p2 p3 - 3 points in 3D space
% plane     - plane formed by the 3 points


    %  A = |1 y1 z1| B = |x1 1 z1| C = |x1 y1 1| D = |x1 y1 z1|
    %      |1 y2 z2|     |x2 1 z2|     |x2 y2 1|     |x2 y2 z2|
    %      |1 y3 z3|     |x3 1 z3|     |x3 y3 1|     |x3 y3 z3|

    A = [1 p1(2) p1(3); 1 p2(2) p2(3); 1 p3(2) p3(3)];
    B = [p1(1) 1 p1(3); p2(1) 1 p2(3); p3(1) 1 p3(3)];
    C = [p1(1) p1(2) 1; p2(1) p2(2) 1; p3(1) p3(2) 1];
    D = [p1(1) p1(2) p1(3); p2(1) p2(2) p2(3); p3(1) p3(2) p3(3)];
    
    plane(1) = det(A);
    plane(2) = det(B);
    plane(3) = det(C);
    plane(4) = det(D);

end

