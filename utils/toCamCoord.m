function [ pt_cam ] = toCamCoord( point, calib_params )
%toCamCoord - given a point in image cooridinates, 
%             converts it to camera cooridnates using calib parameters
% point        - row,col in image coordinates
% calib_params - struct of calibration parameters

    if (length(point) == 2)
        dpx = calib_params.dpx;
        dpy = calib_params.dpy;
        Cx = calib_params.Cx;
        Cy = calib_params.Cy;

        pt_cam(1) = dpx * (point(1) - Cx);
        pt_cam(2) = dpy * (point(2) - Cy);
        pt_cam(3) = calib_params.f;  
    end
    
    if (length(point) == 3)
        % our calibration method
        K = calib_params.int;
        K = [K zeros(3,1)];
        pt_cam = pinv(K)*point(1:3)';
    end
       
end

