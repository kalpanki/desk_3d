function [pa, pb] = lineLineIntersect(shadowTop3d, pencilTop3d)
% shadowTop3d - each column contains the 3D points at the top of shadow
%               for each image        
% pencilTop3d - each column contains the 3D points at the top of actual
%               pencil for each image        

pt1 = shadowTop3d(:,1);
pt2 = pencilTop3d(:,1);
pt3 = shadowTop3d(:,2);
pt4 = pencilTop3d(:,2);

p13 = pt1 - pt3;
p43 = pt4 - pt3;
p21 = pt2 - pt1;

d1343 = p13'*p43;
d4321 = p43'*p21;
d1321 = p13'*p21;
d4343 = p43'*p43;
d2121 = p21'*p21;

denom = d2121 * d4343 - d4321 * d4321;
numer = d1343 * d4321 - d1321 * d4343;

mua = numer / denom;
mub = (d1343 + d4321*mua) / d4343;

pa = pt1 + mua*(pt2 - pt1);
pb = pt3 + mub*(pt4 - pt3);

