function [ shadow_plane ] = getShadowPlane( ...
                            row,col,Ts,x_top_map, x_bot_map, ...
                            y_top, y_bot, calib_params)
%GETSHADOWPLANE Returns the shadow plane PI(t) at a given time 
%   Detailed explanation goes here

    % - consider a pixel - row, col
    % - get ts corresponding to that pixel - shadow times.
    ts = Ts(row,col);
    
    % - for that ts, get the top and bottom reference points:
    %fprintf('checking mapping for: %f %f %f \n', ts, row, col);
    x_top_ref = x_top_map(ts);
    x_bot_ref = x_bot_map(ts);
    
    % - convert the reference points to camera ref frame
    pt1_ref = toCamCoord([x_top_ref y_top 1], calib_params);
    pt2_ref = toCamCoord([x_bot_ref y_bot 1], calib_params);
    pt1 = vectorPlaneIntersect(calib_params.desk, pt1_ref);
    pt2 = vectorPlaneIntersect(calib_params.desk, pt2_ref);
    
    % - create the shadow plane using the 2 points and Source.
    shadow_plane = getPlaneFromPoints(pt1, pt2, calib_params.source);

end